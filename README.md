# Polyphony

Library to facilitate generating different sounds at the same time (polyphony).

Contributing
------------

We welcome contributions, both in the form of issues and in the form of pull requests.
Before opening a pull request, please open an issue first so that you know whether a subsequent
pull request would likely be approved.

If you don’t have a Codeberg account, alternatively, you can contribute via e-mail
(an email address is in the commits). 
But just creating a Codeberg account is probably the easiest.

Unless explicitly stated otherwise, you agree that your contributions are licensed as described
below.

License
-------

`polyphony` is distributed under the terms of the MIT license or the Apache License (Version 2.0), 
at your choice.
For the application of the MIT license, the examples included in the doc comments are not
considered "substatial portions of this Software".
