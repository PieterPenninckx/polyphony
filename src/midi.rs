//! Module for handling raw midi data (behind the `midi` feature)
use super::{EventDispatchClass, EventDispatchClassifier};
use midi_consts::channel_event::*;

/// Used to dispatch polyphonic event to the correct voice, based on the tone of the event.
#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub struct ToneIdentifier(pub u8);

/// Classify a raw midi event based on the tone (e.g. F# and Bb go to different voices).
#[derive(Default)]
pub struct RawMidiEventToneIdentifierDispatchClassifier;

impl EventDispatchClassifier<[u8; 3]> for RawMidiEventToneIdentifierDispatchClassifier {
    type VoiceIdentifier = ToneIdentifier;

    fn classify(&self, event: &[u8; 3]) -> EventDispatchClass<ToneIdentifier> {
        match event[0] & EVENT_TYPE_MASK {
            NOTE_OFF => EventDispatchClass::VoiceSpecific(ToneIdentifier(event[1])),
            NOTE_ON => {
                if event[2] == 0 {
                    // Velocity 0 is considered the same as note off.
                    EventDispatchClass::VoiceSpecific(ToneIdentifier(event[1]))
                } else {
                    EventDispatchClass::AssignNewVoice(ToneIdentifier(event[1]))
                }
            }
            POLYPHONIC_KEY_PRESSURE => EventDispatchClass::VoiceSpecific(ToneIdentifier(event[1])),
            _ => EventDispatchClass::Broadcast,
        }
    }
}
